import { writeFileSync } from "fs";
import path from "path";
import { Column, DataType, Model, Sequelize, Table } from "sequelize-typescript";

// Part 1

const filePathForTask1 = path.resolve(__dirname, "output", "output1.json");

const { MY_STRING } = process.env;

const task1 = () => {
  if (typeof MY_STRING !== "string" || MY_STRING.length === 0) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "MY_STRING не был отправлен" }));
    return;
  }

  const result = MY_STRING.split(" ").length;

  writeFileSync(
    filePathForTask1,
    JSON.stringify({
      input: {
        MY_STRING,
      },
      result,
    })
  );
};

task1();

// Part 2
const filePathForTask2 = path.resolve(__dirname, "output", "output2.json");

const initDB = async () => {
  @Table({})
  class Student extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    surname: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    name: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    birthdayDate: Date;
  }

  const sequelize = new Sequelize({
    dialect: "postgres",
    host: "database",
    port: 5432,
    username: "postgres",
    password: "qwerty",
    database: "sachkov_practice_db",
  });

  sequelize.addModels([Student]);

  try {
    await sequelize.authenticate();
    console.log("Init sequelize OK");
  } catch (error) {
    console.error("Init sequelize error", error);
  }

  const firstStudent = await Student.findOne({
    order: [["birthdayDate", "ASC"]],
  });

  const secondStudent = await Student.findOne({
    order: [["birthdayDate", "DESC"]],
  });

  writeFileSync(filePathForTask2, JSON.stringify({ firstStudent, secondStudent }));
};

initDB();
